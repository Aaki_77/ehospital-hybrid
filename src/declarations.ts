export interface AppPage {
    url: string;
    iosIcon: string;
    mdIcon: string;
    title: string;
    
  }


  export interface Report {
  
    testReportid?: Number;
    testReportName?: string;
    testAppointmentid?: Number;
    patientid?:Number;
    udf1?:String;
    udf2?:String
    udf3?:String
    udf4?:String
    udf5?:String
    udf6?:String
    udf7?:String
    udf8?:String
    udf9?:String
    udf10?:String
    
  }

  export interface prod {
    hospital_subcategory_id?: Number;
    doctorName?: string;
    image?: Blob;
    data?: string;
   
  }

  export interface prod2 { 
    hospital_subcategory_id?: Number;
    doctorName?: string;
    image?: Blob;
    data?: string;
  }

