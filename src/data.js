export default  {
    products: [
        {
            _id:'1',
            name: 'Yoga',
            image:'images/Home/Yoga.jpg',
            
            rating: 5
      
        },
        {
            _id:'2',
            name: 'Ayurvedic',
            image:'images/Home/Ayurvedic.jpg',
            
            rating: 8
      
        },
        {
            _id:'3',
            name: 'Fitness',
            image:'images/Home/Fitness.jpg',
            
            rating: 9
      
        },
        {
            _id:'4',
            name: 'Homeopathic',
            image:'images/Home/Homeopathic.jpg',
            
            rating: 9
      
        },
        {
            _id:'5',
            name: 'Allopathy',
            image:'images/Home/Allopathy.jpg',
            
            rating: 9
      
        }
    ]
}