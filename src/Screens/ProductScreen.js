import React from 'react';
import {Link} from 'react-router-dom';
import data from '../data';


function ProductScreen(props){
console.log(props.match.params.id);

const product = data.products.find(x => x._id === props.match.params.id);
    return <div>
<div>
    <Link to="/">Back to result</Link>
    <div className="details"></div>
    <div className="details-image"></div>

    <img src={product.image} alt ="product"></img>
    <div className="details-info">
    <ul>

        <li>
           <h4> {product.name}</h4>
        </li>
        <li>
            {product.rating}stars
            </li>
    </ul>
    </div>
    <h1>{product.name}</h1>
    </div>
    </div>
}
export default ProductScreen;