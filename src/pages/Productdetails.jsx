
import { IonPage, IonContent, IonGrid, IonRow, IonCol,IonButton, IonCard } from '@ionic/react';


import React, { Component } from 'react';
import axios from 'axios';

class Productdetails extends Component {
    state = {
        persons : [],
    };
    componentDidMount() {
        const { doctorDetailsId } = this.props.match.params
        axios.get(`http://localhost:8080/ddd/${doctorDetailsId}`).then(res => {
            console.log(res)
            this.setState({persons: res.data});
        }
)}


    render(){
        return(
            <IonPage>
            <IonContent>
           
            
                {
                    this.state.persons.map(person =>
                        <div key={person.doctorDetailsId}>
                   
                   <div className="details">
                        

<IonCol size="5">
<div className="details-image">

  < img width = "350" height = "550" src=  {"data:image/jpeg;base64," +  person.image }/>

</div>
</IonCol>



<IonGrid>
<IonCol  >
<div className="details-info">
<ul>
<li>
<h4>{person.doctorName}</h4>
</li>
<li>{person.udf1}</li>
{/* <li>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

<span className="fa fa-star checked"></span>
<span className="fa fa-star checked"></span>
<span className="fa fa-star checked"></span>
<span className="fa fa-star"></span>
<span className="fa fa-star"></span>
</li> */}
<li>
Fee : ₹ <b>{person.doctorFee}</b>
</li>
<li>
Description :
<div>{person.udf2}</div>
</li>

</ul>


</div>
</IonCol>
</IonGrid>
<br></br>

<IonGrid>
<IonCol >
<IonCard>
<div className="details-action">
<ul>

<li>
<IonButton color="success" href="/Payment">Book an Appointment</IonButton>
<IonButton  color="warning" href="/page/Home">Change Appointment</IonButton>
</li>

</ul>

</div> 
</IonCard>
</IonCol>
</IonGrid>

</div>
</div>
                        )
                }
           
           

            &nbsp;&nbsp;
                        &nbsp;&nbsp;             &nbsp;&nbsp;
                        &nbsp;&nbsp;
            </IonContent>
            </IonPage>
        )
    }
}

export default Productdetails;