import React,{useCallback, useEffect,useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
    IonFab, IonFabButton, IonIcon, IonGrid, IonRow, IonCol, IonImg, IonActionSheet, IonItem, IonBadge, IonButton, IonMenuButton } from '@ionic/react';
    import { RouteComponentProps } from 'react-router';
import Axios,{AxiosResponse} from 'axios';
import { image, key, map } from 'ionicons/icons';
import { isLabeledStatement } from 'typescript';
import data from '../data';
//import './User.css';

interface TestBookingProps extends RouteComponentProps<{hospitalId:string}>{}
const api= Axios.create({
  baseURL: 'http://192.168.2.6:8080/labList/'
})
const TestBooking: React.FC<TestBookingProps> = ({match}) => {
  const [items, setitems] = useState<Lab[]>();
  console.log("match",match.params.hospitalId);
  var apiString = '/' + match.params.hospitalId;

  api.get(apiString) .then((response: AxiosResponse) => {
    console.log("res",response.data);
    setitems( response.data );
});
    
    
   


return(
    
        <IonPage>
            
          <IonHeader>
           
              <IonTitle text-center>Test Booking</IonTitle>
              <IonMenuButton />
          
          </IonHeader>
          <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Test Booking</IonTitle>
          </IonToolbar>
        </IonHeader>
          <IonContent >
          
         <IonGrid>
         <IonRow>
         {items?.map((item, index) => (
           
           <IonCol sizeMd={'3'} key={index}>
           
            <h4>Lab Test</h4>
            &nbsp;&nbsp;
            
         <table>
           <tbody>      
        <tr>
          <td>Lab Test Name:</td>
          <td>&nbsp;&nbsp;</td>
          <td>{item.labTestName}  </td>
        </tr>  
        &nbsp;&nbsp;
      
        &nbsp;&nbsp;
        <tr>
          <td>Test Description:</td>
          <td>&nbsp;&nbsp;</td>
          <td>{item.testDescription}  </td>
        </tr>  
        &nbsp;&nbsp;
        <tr>
          <td>Price:</td>
          <td>&nbsp;&nbsp;</td>
          <td>{item.testAmount}  </td>
        </tr>  
        &nbsp;&nbsp;

        <tr>
          <td></td>
          <td>&nbsp;&nbsp;</td>
          <td>{item.udf1}  </td>
        </tr>       
        &nbsp;&nbsp;
        </tbody>
        </table> 
        <IonButton href="/page/Home">Payment</IonButton>
        <IonButton href="/page/abc">TestList</IonButton>
        </IonCol>
         ))}
        
         </IonRow>
       </IonGrid>
       </IonContent>
         </IonPage> 
        
);
};

export default TestBooking;

export interface Lab {
  labTestName?: string;
  hospitalId?: Number;
  testAmount?: Number;
  testDescription?: String;
  udf1?:String
  image?: String
}



