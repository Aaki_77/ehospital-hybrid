import React,{useState,useEffect} from 'react';
import { IonContent, IonListHeader,IonHeader, IonToolbar, IonTitle, IonMenuButton, IonApp, IonGrid, IonRow } from '@ionic/react';
import Styles from './Appointments.module.css';
import Axios,{AxiosResponse} from 'axios';
import {Link} from 'react-router-dom';
import { url } from 'inspector';
const api= Axios.create({
  baseURL: 'http://192.168.2.6:8080/getPatientAppointmentHistory'
})
const Appointments: React.FC = () => {
  const [items, setitems] = useState<Appo[]>();
  useEffect(() => {
    
    api.get('/')
    .then((response: AxiosResponse) => {
        console.log("res",response.data);
        setitems( response.data );
    });
}, []);


  return(
  <IonApp>
    <IonHeader translucent>  
      <IonToolbar>  
        <IonListHeader>
        <IonMenuButton />
        <IonTitle>Appointments</IonTitle>  
        </IonListHeader>
      </IonToolbar>  
    </IonHeader>    
  <IonContent>
    <IonGrid>
        <IonRow style={{overflowX:"scroll"}}>
           
            <table>
                <thead>
                    <tr>
                        <th colSpan={8} className={`${Styles.setth}`}>Appointment List</th>
                    </tr>
                    <tr>
                       <th className={`${Styles.setth}`}>Patient Appointment Id </th>
                        <th className={`${Styles.setth}`}>Appointment Start Time</th>
                        <th className={`${Styles.setth}`}>Appointment End Time</th>
                        <th className={`${Styles.setth}`}>Doctor Detail Id</th>
                        <th className={`${Styles.setth}`}>Video Link</th>
                        <th className={`${Styles.setth}`}>Appointment Status</th>
                    </tr>
                </thead>   
                {items?.map((item, index) => (
                
                <tbody>
                  
                    <tr>
                    <td className={`${Styles.settd}`}>{item.patientAppointmentHistoryId}</td>
                        <td className={`${Styles.settd}`}>{item.appointmentStartTime}</td>
                        <td className={`${Styles.settd}`}>{item.appointmentEndTime}</td>
                        <td className={`${Styles.settd}`}>{item.doctorDetailsId}</td>
                       <td className={`${Styles.settd}`}> <a href="https://www.youtube.com/">{item.videoLink}</a> </td> 
                        <td className={`${Styles.settd}`}>{item.appointmentStatus}</td>
                    </tr>
                   
                </tbody>   
                 ))}
            </table>
            
        </IonRow>
        
    </IonGrid>
  </IonContent>
);
     
  </IonApp>
  )}

export default Appointments;

export interface Appo{
  patientAppointmentHistoryId?:Number;
  appointmentStartTime?:Date;
  appointmentEndTime?:Date;
  expireStatus?:String;
  patientsAppointmentDetailsId?:Number;
  appointmentIsDone?:String;
  doctorDetailsId?:Number;
  videoLink?:String;
  appointmentStatus?:String;
  patientid?:Number;
}
