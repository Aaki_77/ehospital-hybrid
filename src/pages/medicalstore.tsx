import { IonApp,IonHeader,IonButtons,IonMenuButton,IonToolbar,IonTitle, IonContent, IonSearchbar, IonGrid, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle } from '@ionic/react';
import React from 'react';


const MedicalStore : React.FC = () => {
    return(
        <IonApp>
            <IonHeader>
                <IonToolbar>
                <IonButtons slot="start">
                    <IonMenuButton />
                </IonButtons>
                <IonTitle>Medical Store</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonSearchbar></IonSearchbar>
                <IonGrid>
                    <IonRow>
                        <IonCol size="6" sizeMd="3">
                            <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px",width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/910016/tri_activ_instant_hand_sanitizer_500_ml_0_1.jpg" />
                                    <IonCardSubtitle style={{height:"70px"}}>Tri-Active instant Hand Santizer 500ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>
                                </IonCardHeader>
                            </IonCard>

                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px",width:"100%"}}  src="https://www.netmeds.com/images/product-v1/150x150/912708/floh_instant_hand_sanitizer_with_70_ethyl_alcohol_spearmint_essential_oil_5_litre_0_0.jpg" alt=""/>
                                    <IonCardSubtitle style={{height:"70px"}}>FLOH Instant Hand Sanitizer with 70% Ethyl Alcohol - Spearmint</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>
                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6"sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px",width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/909365/cura_home_sanitizer_500_ml_0_0.jpg" alt=""/>
                                    <IonCardSubtitle style={{height:"70px"}}>Cura Home Sanitizer 500 ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>
                                    
                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px",width:"100%"}}  src="https://www.netmeds.com/images/product-v1/150x150/907575/cura_hand_sanitizer_200_ml_0_0.jpg" alt=""/>
                                    <IonCardSubtitle style={{height:"70px"}}>Cura Hand Sanitizer 200 ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px",width:"100%"}}  src="https://www.netmeds.com/images/product-v1/150x150/907574/cura_hand_sanitizer_100_ml_0_0.jpg" alt=""/>
                                    <IonCardSubtitle style={{height:"70px"}}>Cura Hand Sanitizer 200 ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px",width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/914762/ciphands_professional_chg_handrub_solution_500_ml_0_1.jpg" alt=""/>                                
                                    <IonCardSubtitle style={{height:"70px"}}>Cura Hand Sanitizer 200 ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/908599/ciphands_antiseptic_hand_sanitizer_100_ml_0_1.jpg" alt=""/>
                                    <IonCardSubtitle style={{height:"70px"}}>Ciphands Professional CHG Handrub Solution 500 ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/908599/ciphands_antiseptic_hand_sanitizer_100_ml_0_1.jpg" alt=""/>
                                    <IonCardSubtitle style={{height:"70px"}}>Ciphands Antiseptic Hand Sanitizer 100 ml</IonCardSubtitle>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/892919/lifebuoy_nature_handwash_750_ml_refill_pack_0_0.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/913277/cura_herbal_hand_wash_500_ml_0_0.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/828322/dettol_liquid_handwash_original_pack_of_3_x_175_ml_0.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/909752/fyne_hand_sanitizer_5_litre_0_0.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/812638/accu_chek_instant_s_blood_glucose_monitor_with_free_10_test_strips_0_4.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/854053/onetouch_select_plus_simple_monitor_0.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>

                                </IonCardHeader>
                         
                            </IonCard>
                        </IonCol>
                        <IonCol size="6" sizeMd="3">
                        <IonCard href="#">
                                <IonCardHeader>
                                    <img style={{height:"100px", width:"100%"}} src="https://www.netmeds.com/images/product-v1/150x150/408327/onetouch_verio_flex_meter_0.jpg" alt=""/>
                                    <IonCardSubtitle color="warning">RS. 237.50</IonCardSubtitle>
                                </IonCardHeader>
                          
                            </IonCard>
                        </IonCol>
                    </IonRow>
                </IonGrid>

            </IonContent>

        </IonApp>
    );   
}


export default MedicalStore