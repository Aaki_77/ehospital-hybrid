import {
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonMenu,
    IonMenuToggle,
    IonNote,
    
    
    
  } from '@ionic/react';
  import React from 'react';
  import { useLocation } from 'react-router-dom';
  import { calendarOutline, calendarSharp, todayOutline, todaySharp, reorderThreeOutline, reorderThreeSharp, homeOutline, homeSharp, bookOutline, bookSharp,  medkitOutline, medkitSharp,bookmarkOutline, storefrontOutline, storefrontSharp,folderOutline,folderSharp,peopleOutline,peopleSharp } from 'ionicons/icons';
  import {personCircle as personIcon } from 'ionicons/icons';
  import { logoEdge as logoIcon} from 'ionicons/icons';
  
  
  interface Products {
    _id: string;
    name: string;
    image: string;
    rating: string;
  }

    const products: Products[] = [
        {
            _id:'Ab',
            name: 'Yoga',
            image:'images/Home/Yoga.jpg',        
            rating: 'five'
      
        },
        {
            _id:'cd',
            name: 'Ayurvedic',
            image:'images/Home/Ayurvedic.jpg',         
            rating: 'eight'
      
        },
        {
            _id:'3',
            name: 'Fitness',
            image:'images/Home/Fitness.jpg',        
            rating: 'Nine'
      
        },
        {
            _id:'4',
            name: 'Homeopathic',
            image:'images/Home/Homeopathic.jpg',  
            rating:'nine'
      
        },
        {
            _id:'5',
            name: 'Allopathy',
            image:'images/Home/Allopathy.jpg',
            rating: 'nine'
        }
    ];
  const Homes: React.FC = () => {
    const location = useLocation();
  
    return (
      <IonMenu contentId="homes" type="overlay">
        <IonContent>
          <IonList id="ehospital">
        
            <IonListHeader>
            <IonIcon icon={logoIcon} />
              ehospital</IonListHeader>
  
              <br></br> 
  
              <IonIcon icon={personIcon}/>
            <IonNote>username@ehospital.com
              </IonNote>
            {products.map((products, index) => {
              return (
                <IonMenuToggle key={index} autoHide={false}>
                  <IonItem className={location.pathname === products.image ? 'selected' : ''} href={products.image} routerDirection="none" lines="none" detail={false}>
                    <IonIcon slot="start" ios={products._id} md={products.name} />
                    <IonLabel>{products.rating}</IonLabel>
                  </IonItem>
                </IonMenuToggle>
              );
            })}
          </IonList>
  
          
        </IonContent>
      </IonMenu>
    );
  };
  
  
  
  export default Homes;
  