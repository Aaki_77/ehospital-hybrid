import { useEffect,useState } from "react";
import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonGrid, IonRow, IonCol, IonButton, IonMenuButton } from "@ionic/react";
import React from 'react';
import {  Report } from "../declarations";
import axios from "axios";
import Axios,{AxiosResponse} from 'axios';


const api= Axios.create({
  baseURL: 'http://localhost:8080/getTest'
})

const TestReport: React.FC = () => {
  
    const [items, setitems] = useState<Report[]>();
  
  
    useEffect(() => {
    
      api.get('/')
      .then((response: AxiosResponse) => {
          console.log("res",response.data);
          setitems( response.data );
      });
  }, []);

  
  return(
      
          <IonPage>
               <IonMenuButton />
            <IonHeader>
              <IonToolbar>
                <IonTitle text-center>Test Reports</IonTitle>
              </IonToolbar>
            </IonHeader>
            <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Test Reports</IonTitle>
            </IonToolbar>
          </IonHeader>
            <IonContent  >
 
           <IonGrid>
           <IonRow>
           {items?.map((item, index) => (
             <IonCol size="10" key={index}>
             

          <table id="customers">
          <caption>Lab Test Report</caption>
           &nbsp;&nbsp;
          <tr>
          &nbsp;&nbsp;
          <th>Test Report name</th>
          &nbsp;&nbsp;
          <th>Test Appointment id</th>
          &nbsp;&nbsp;
          <th>Patient id</th>
          &nbsp;&nbsp;
          <th>udf</th>
          &nbsp;&nbsp;
          </tr>

          <tr>
          &nbsp;&nbsp;
    <td>{item.testReportName}</td>
    &nbsp;&nbsp;
    <td>{item.testAppointmentid}</td>
    &nbsp;&nbsp;
    <td>{item.testAppointmentid}</td>
    &nbsp;&nbsp;
    <td>{item.patientid}  </td>
  
    <td>{item.udf1}  </td>
    
  </tr>
          </table>
          
           
          &nbsp;&nbsp;
          

        
          
          <br></br>

          </IonCol>
           ))}
          
           </IonRow>
         </IonGrid>


         </IonContent>
           </IonPage> 
           
          
  );
  };
  
  
  export default TestReport;