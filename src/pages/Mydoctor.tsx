import { IonApp, IonContent,IonButtons,IonMenuButton, IonHeader, IonTitle, IonToolbar, IonGrid, IonRow } from '@ionic/react';
import Axios,{AxiosResponse} from 'axios';
import React,{useState,useEffect} from 'react';
import Styles from './Mydoctor.module.css';
const api= Axios.create({
  baseURL: 'http://192.168.2.6:8080/doctorDetails'
})
const Mydoctor: React.FC = (props) => {

  const [items, setitems] = useState<Doctor[]>();
  useEffect(() => {
    
    api.get('/')
    .then((response: AxiosResponse) => {
        console.log("res",response.data);
        setitems( response.data );
    });
}, []);
console.log("item",items);
 
 
  
  return (
    <IonApp>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>My Doctor</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
            <IonRow style={{overflowX:"scroll"}}>
                <table>
                    <thead>
                        <tr>
                       
                        <th className={`${Styles.setth}`} rowSpan={2}>DoctorName</th>
                        <th className={`${Styles.setth}`} colSpan={1}>Address</th>
                        <th className={`${Styles.setth}`} rowSpan={2}>DoctorFee</th>
                      
                        
                      </tr>
                    </thead>
                    <tbody>
                      {
                         items?.map((getdata) =>
                        // ds.map((getdata) =>
                        // ds.map((getdata) => (
                          
                          <tr>
                            {console.log("docotrname",getdata.doctorName)}
                            {/* <tr key={getdata.slno}> */}
                            {/* {getdata.slno ? <td className={`${Styles.settd}`}>{getdata.slno}</td> : <td className={`${Styles.settd}`}>Any</td>} */}
                            {getdata.doctorName ? <td className={`${Styles.settd}`}>{getdata.doctorName}</td> : <td className={`${Styles.settd}`}></td>}
                            {getdata.doctorAddress ? <td className={`${Styles.settd}`}>{getdata.doctorAddress}</td> : <td className={`${Styles.settd}`}></td>}
                           
                            {getdata.doctorFee ? <td className={`${Styles.settd}`}>{getdata.doctorFee}</td> : <td className={`${Styles.settd}`}>Any</td>}
                           
                          </tr>
                        )
                      }
                    </tbody>

                </table>
            </IonRow>
        </IonGrid>
      </IonContent>
    </IonApp>
  );
};

export default Mydoctor;

export interface Doctor{
  doctorDetailsId?:Number,
  doctorName?:String,
  doctorAddress?:String,
  hospitalSubcategoryId?:Number,
  active?:boolean,
  stateId?:Number,
  cityId?:Number,
  pincode?:String,
  countryId?:Number,
  doctorFee?:DoubleRange,
  udf1?:String,
  udf2?:String,
  udf3?:String,
  udf4?:String,
  udf5?:String,
  aggrementId?:Number,
  imagePath?:String,
  charges?:DoubleRange,
  chargesType?:String,
  noOfPatient?:Number,
  fromTime?:String,
  endTime?:String,
  activeTime?:Number,
  chargesApplicable?:boolean,
  onePatientsValidity?:Number
}
