 import Page from './pages/Page';
import React,{Component} from 'react';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import {Route } from 'react-router-dom';
//import { Link, Redirect, Route } from 'react-router-dom';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
// import Mydoctor from './pages/Mydoctor';
// import Home from './pages/Home';
// import Yoga from './pages/Yoga';
// import Appointments from './pages/Appointments';
// import Orders from './pages/orders';
// import Cart from './Cart';
// import MedicalStore from './pages/medicalstore';
// import Shoppingcart from './pages/Shoppingcart';
// import ShoppingIndex from './pages/ShoppingIndex';
import Page from './Page';
import Mydoctor from './Mydoctor';
import Home from './Home';
import Yoga from './Yoga';
import Appointments from './Appointments';
import ShoppingIndex from './ShoppingIndex';
import Menu from '../components/Menu';
import Logout from '../Login/Logout';
import Ayurvedic from './Ayurvedic';
import Homeopathic from './Homeopathic';
import Fitness from './Fitness';
import Allopathy from './Allopathy';
import TestReport from './TestReports';
import HealthFile from './HealthFile';
import Abc from './abc';
import TestBooking from './TestBooking';
import Signup from './Signup';
import Productdetails from './Productdetails';
import Products from '../ShoppingComponents/Products';
// import Menu from './Menu';


class Main extends Component {

 
render(){
  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
          <Route path="/page/Home" component={Home} exact /> 
            <Route path="/page/:name" component={Page} exact />
            <Route path="/page/Mydoctor" component={Mydoctor} exact />
            <Route path="/Login/Logout" component={Logout} exact/>
           
            <Route path="/product/Ayurvedic" component={Ayurvedic} exact/>
            <Route path="/product/Yoga" component={Yoga} exact/>
            <Route path="/product/Homeopathic" component={Homeopathic} exact/>
            <Route path="/product/Fitness" component={Fitness} exact/>
            <Route path="/product/Allopathy" component={Allopathy} exact/>
            <Route path="/page/TestReports" component={TestReport} exact />
            <Route path="/page/Appointments" component={Appointments} exact />
            {/* <Route path="/page/Orders" component={Orders} exact/> */}
            <Route path="/page/medicalstore" component={ShoppingIndex} exact />
            <Route path="/page/Health File" component={HealthFile} exact />
            <Route path="/page/abc" component={Abc} exact /> 
            <Route path="/page/TestBooking/:hospitalId" component={TestBooking} exact />
            <Route path="/page/Appointments" component={Appointments} exact />
            <Route path="/page/Home" component={Home} exact />
          <Route path="/Productdetails/:doctorDetailsId" component={Productdetails} exact/>

            <Route path="/Products/:hospitalcategoryid" component={Products} exact/>
            {/* <Route path="/page/Menu" component={Menu} exact /> */}


          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
}
};

export default Main;
