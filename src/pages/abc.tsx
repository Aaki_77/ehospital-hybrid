import React,{useState, useEffect} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
   IonGrid, IonRow, IonCol, IonMenuButton } from '@ionic/react';

  // import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
  //   IonList, IonSlides, IonSlide, IonGrid, IonRow, IonCol, IonImg, IonActionSheet } from '@ionic/react';
  
import Axios,{AxiosResponse} from 'axios';

import './User.css';
//import { RouteComponentProps } from 'react-router';

const api= Axios.create({
  baseURL: 'http://192.168.2.6:8080/labList'
})
const Abc: React.FC = () => {
 
  const [items, setitems] = useState<Lab[]>();
  useEffect(() => {
  api.get('/')
  .then((response: AxiosResponse) => {
      console.log("res",response.data);
      setitems( response.data );
  });
}, []);
console.log("item",items);

  
return(
    
        <IonPage>
             <IonMenuButton />
          <IonHeader>
            <IonToolbar>
              <IonTitle text-center>Test Booking</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Test Booking</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonGrid>
        
            <IonRow>
            {items?.map((item, index) => (
            <IonCol sizeMd={'3'} size={'12'} sizeSm={'6'} key={index}>
                <h5>{item.labTestName}</h5>
                <a href={"/page/TestBooking/"+item.hospitalId} ><img width="400" height="300" src={"data:image/jpeg;base64," + item.image} alt="pic1"></img></a>
            </IonCol>
            ))}
            </IonRow>
         
          </IonGrid>
        </IonContent>
         ))
         </IonPage>    
);           
};
export default Abc;

export interface Lab {
 
  labTestListId?: Number,
  labTestName?:String,
  image?: String,
  hospitalId?:Number
 
 
}












