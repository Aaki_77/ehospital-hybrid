import {
  IonButton,
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList, 
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
  
} from '@ionic/react';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { calendarOutline, calendarSharp, todayOutline, todaySharp, reorderThreeOutline, reorderThreeSharp, homeOutline, homeSharp, bookOutline, bookSharp,  medkitOutline, medkitSharp,bookmarkOutline, storefrontOutline, storefrontSharp,folderOutline,folderSharp,peopleOutline,peopleSharp, menuSharp } from 'ionicons/icons';
import './Menu.css';
import {personCircle as personIcon } from 'ionicons/icons';
import { logoEdge as logoIcon} from 'ionicons/icons';


interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
} 
const logoutClick = () => {
  //localStorage.setItem('loggedin','f');
  localStorage.removeItem('loggedin');
  localStorage.removeItem('username');
  localStorage.removeItem('useremail');
};


const appPages: AppPage[] = [
  {
    title: 'Home',
    url: '/page/Home',
    iosIcon: homeOutline,
    mdIcon: homeSharp
  },
  {
    title: 'Appointments',
    url: '/page/Appointments',
    iosIcon: calendarOutline,
    mdIcon: calendarSharp
  },
  {
    title: 'Test Bookings',
    url: "/page/abc",
    iosIcon: todayOutline,
    mdIcon: todaySharp
  },
  {
    title: 'Orders',
    url: '/page/Orders',
    iosIcon: reorderThreeOutline,
    mdIcon: reorderThreeSharp
  },
  {
    title: 'Test Reports',
    url: '/page/TestReports',
    iosIcon: bookOutline,
    mdIcon: bookSharp
  },
  {
    title: 'Vaccination',
    url: '/page/Vaccination',
    iosIcon: medkitOutline,
    mdIcon: medkitSharp
  },
  {
    title: 'Medical Store',
    url: '/page/medicalstore',
    iosIcon: storefrontOutline,
    mdIcon: storefrontSharp
  },
  {
    title: 'Health File',
    url: '/page/Health File',
    iosIcon: folderOutline,
    mdIcon: folderSharp
  },
  {
    title: 'My Doctors',
    url: '/page/Mydoctor',
    iosIcon: peopleOutline,
    mdIcon: peopleSharp
  },
  // {
  //   title: 'Logout',
  //   url: '/Login/Logout',
  //   iosIcon: peopleOutline,
  //   mdIcon: peopleSharp,
    
  // },
  
];

const labels = ['Help Center', 'Read about health', 'Settings'];


const Menu: React.FC = () => {
  const location = useLocation();

  return (
    <IonMenu contentId="main" type="overlay">


      <IonContent>
        <IonList id="ehospital">
      
          <IonListHeader>
          <IonIcon icon={logoIcon} />
          MED-E</IonListHeader>

            <br></br> 

            {/* <IonIcon icon={personIcon}/>
          <IonNote>MED-E
            </IonNote> */}
            <IonMenu side="end" type="push"></IonMenu>
          {appPages.map((appPage, index) => {
            return (
             
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} href={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
              
            );
          })}
        </IonList>
      
                {/* <IonButton href = '/page/Login' onClick={logoutClick} >Logout</IonButton>  */}
                  
                
        <IonList id="labels-list">
          <IonListHeader>Communication</IonListHeader>
          {labels.map((label, index) => (
            <IonItem lines="none" key={index}>
              <IonIcon slot="start" icon={bookmarkOutline} />
              <IonLabel>{label}</IonLabel>
            </IonItem>
          ))}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
  