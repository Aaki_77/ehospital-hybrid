// import Page from './pages/Page';
import React,{Component, useState, useContext} from 'react';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Link, Redirect, Route } from 'react-router-dom';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
// import Login from './Login/Login';
import Main from './pages/Main';
import Menu from './components/Menu';
import Page from './pages/Page';
import Mydoctor from './pages/Mydoctor';
import { logOut } from 'ionicons/icons';
// import Home from './ShoppingComponents/Home';
import Ayurvedic from './pages/Ayurvedic';
import Yoga from './pages/Yoga';
import Homeopathic from './pages/Homeopathic';
import Fitness from './pages/Fitness';
import Allopathy from './pages/Allopathy';
import TestReport from './pages/TestReports';
import Appointments from './pages/Appointments';
// import ShoppingIndex from './pages/ShoppingIndex';
import HealthFile from './pages/HealthFile';
import Abc from './pages/abc';
import TestBooking from './pages/TestBooking';
import Productdetails from './pages/Productdetails';
import Home from './pages/Home';
import Products from './pages/Products';
import Hom from './pages/Hom';
// import Products from './ShoppingComponents/Products';
// import Signup from './pages/Signup';


interface IUserManager {
  setIsLoggedIn: Function;
}

const user: IUserManager = {
  setIsLoggedIn: () => {}
 
};

export const UserContext = React.createContext<IUserManager>(user);
 

const App: React.FC = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const user = useContext(UserContext);

  user.setIsLoggedIn = setIsLoggedIn;
  return(
    // <IonApp> 
    // <IonReactRouter>
          //* <Route   path='/page/Login' component={Login} /> */
          ///* <Route  path='/pages/Signup' component={Signup}/> */
  //          {/* <Route  path="/" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact={true} /> 
  //          <Route path="/page/:name" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Mydoctor" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/Login/Logout" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Home" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/product/Ayurvedic" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/product/Yoga" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/product/Homeopathic" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/product/Fitness" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/product/Allopathy" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/TestReports" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Appointments" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Orders" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/medicalstore" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Health File" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/abc" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/TestBooking/:hospitalId" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Appointments" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //           <Route path="/page/Home" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />
  //         <Route path="/Productdetails/:doctorDetailsId" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact />

  //           <Route path="/Products/:hospitalcategoryid" component={(localStorage.getItem('loggedin') == 't') ? Main : Login} exact /> */}

            

  // //   </IonReactRouter>
  // // </IonApp>
<IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">

            <Route path="/page/:name" component={Page} exact />
            <Route path="/page/Mydoctor" component={Mydoctor} exact />
            {/* <Route path="/Login/Logout" component={logOut} exact/> */}
            <Route path="/page/Hom" component={Home} exact /> 
            <Route path="/product/Ayurvedic" component={Ayurvedic} exact/>
            <Route path="/product/Yoga" component={Yoga} exact/>
            <Route path="/product/Homeopathic" component={Homeopathic} exact/>
            <Route path="/product/Fitness" component={Fitness} exact/>
            <Route path="/product/Allopathy" component={Allopathy} exact/>
            <Route path="/page/TestReports" component={TestReport} exact />
            <Route path="/page/Appointments" component={Appointments} exact />
            {/* <Route path="/page/Orders" component={Orders} exact/> */}
            {/* <Route path="/page/medicalstore" component={ShoppingIndex} exact /> */}
            <Route path="/page/Health File" component={HealthFile} exact />
            <Route path="/page/abc" component={Abc} exact /> 
            <Route path="/page/TestBooking/:hospitalId" component={TestBooking} exact />
            <Route path="/page/Appointments" component={Appointments} exact />
            <Route path="/page/Home" component={Home} exact />
          <Route path="/Productdetails/:doctorDetailsId" component={Productdetails} exact/>

            <Route path="/Products/:hospitalcategoryid" component={Products} exact/>
            <Route path="/Hom" component={Hom} exact/>
            {/* <Route path="/page/Menu" component={Menu} exact /> */}
            <Redirect exact from="/" to="page/Home" />

          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>


);
};


export default App;

